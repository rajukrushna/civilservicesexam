from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView
from .models import Syllabus, MainsQuestionPaper, PreviousMCQ, Subject


# Create your views here.
class HomePageView(TemplateView):
    template_name = 'home.html'


class PrelimsPageView(TemplateView):
    template_name = 'prelims/prelims.html'


class PrelimsSyllabusView(ListView):
    model = Syllabus
    template_name = 'prelims/prelimssyllabus.html'


class PrelimsPapersView(TemplateView):
    template_name = 'prelims/prelimspreviouspapers.html'


class PrelimsTrendsView(TemplateView):
    template_name = 'prelims/prelimstrends.html'


class MainsPageView(TemplateView):
    template_name = 'mains/mains.html'


class MainsSyllabusView(ListView):
    model = Syllabus
    template_name = 'mains/mainssyllabus.html'


class MainsSyllabusDetailView(DetailView):
    model = Syllabus
    template_name = 'mains/mainssyllabusdetail.html'


class MainsPapersView(TemplateView):
    template_name = 'mains/mainspreviouspapers.html'
