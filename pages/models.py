from django.db import models


class Syllabus(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    type = models.CharField(max_length=200)


class Subject(models.Model):
    subject_name = models.CharField(max_length=200)

    def __str__(self):
        return self.subject_name


class PreviousMCQ(models.Model):
    year = models.IntegerField(max_length=4)
    qno = models.IntegerField(max_length=3)
    qtext = models.TextField()
    ans = models.CharField(max_length=1)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, blank=True)
    explanation = models.TextField(blank=True)


class MainsQuestionPaper(models.Model):
    year = models.IntegerField(max_length=4)
    paper = models.CharField(max_length=100)
    content = models.TextField()


