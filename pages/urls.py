from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('prelims/', views.PrelimsPageView.as_view(), name='prelims'),
    path('prelims/syllabus/', views.PrelimsSyllabusView.as_view(), name='prelimssyllabus'),
    path('prelims/previouspapers/', views.PrelimsPapersView.as_view(), name='prelimspreviouspapers'),
    path('prelims/trends/', views.PrelimsTrendsView.as_view(), name='prelimstrends'),
    path('mains/', views.MainsPageView.as_view(), name='mains'),
    path('mains/syllabus/', views.MainsSyllabusView.as_view(), name='mainssyllabus'),
    path('mains/syllabus/<int:pk>/', views.MainsSyllabusDetailView.as_view(), name='mainssyllabusdetail'),
    path('mains/previouspapers/', views.MainsPapersView.as_view(), name='mainspreviouspapers')
]