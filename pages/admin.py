from django.contrib import admin
from .models import Subject, PreviousMCQ, MainsQuestionPaper, Syllabus


class SubjectAdmin(admin.ModelAdmin):
    list_display = ('subject_name',)

class SyllabusAdmin(admin.ModelAdmin):
    list_display = ('title',)


admin.site.register(Subject, SubjectAdmin)
admin.site.register(PreviousMCQ)
admin.site.register(MainsQuestionPaper)
admin.site.register(Syllabus, SyllabusAdmin)
